const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
//const userRoutes = require("./routes/user");

const userController = require('./routes/userController');

const app = express();
const port = process.env.PORT || 3000;

//midleware 
app.use(express.json());
//app.use('/api', userRoutes);

//listar todos los usuarios

app.get('/', userController.listaUsuarios);

//listar un usuario por ID

app.get('/:id', userController.listaPorId);

//crear un usuario

app.post('/', userController.crearUsuario);

//actualizar usuario

app.put('/:id', userController.actualizar);


app.patch('/:id', userController.actualizar);

//eliminar usuario

app.delete('/:id', userController.eliminar);

//mongodb conexion
mongoose
    .connect(process.env.MONGODB_URI)
    .then(() => console.log('Conectado con MongoDB Atlas'));

app.listen(port, () => console.log('escuchando por el puerto', port));
